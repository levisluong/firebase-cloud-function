const functions = require("firebase-functions");

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
// The Firebase Admin SDK to access Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

// Listens for new entity added to /entities/:documentId and run the function to transfer text
exports.transformTextValue = functions.firestore.document('/entities/{documentId}')
    .onCreate((snap, context) => {
        // Grab the current value of what was written to Firestore.
        const original = snap.data().text;

        let newValue = original
        newValue = newValue.trim(); // remove space arround
        newValue = newValue.replace(/\s+/g, ' ');    // remove continous space
        newValue = newValue[0].toUpperCase() + newValue.slice(1);   // Upper Fist character

        return snap.ref.set({ text: newValue }, { merge: true });
    });